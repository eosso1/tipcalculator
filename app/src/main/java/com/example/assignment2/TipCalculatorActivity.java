package com.example.assignment2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

public class TipCalculatorActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tip_calculator);

        Button button = (Button) findViewById(R.id.button);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Spinner spinner;
                EditText input;
                try {
                    String spinVal;
                    spinner = findViewById(R.id.spinner);
                    input = findViewById(R.id.editText);
                    spinVal = spinner.getSelectedItem().toString();
                    final double tipVal = Double.parseDouble(spinVal);
                    String temp = input.getText().toString();
                    final double check = Double.parseDouble(temp);

                    final TextView result = (TextView) findViewById(R.id.result);

                    calculate(tipVal, check, result);
                }
                catch(NumberFormatException ex) {
                    final TextView result = (TextView) findViewById(R.id.result);
                    result.setText("Free dinner? No tip needed!");
                }
            }
        });
    }

    private void calculate(double tip, double amnt, TextView result) {
        double total = (amnt * tip) + amnt;
        total = (double) Math.round(total*100)/100;

        result.setText("Your total including tip is: $: " + total);
    }


}
